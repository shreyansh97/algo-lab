#include <stdio.h>
#include <stdlib.h>

typedef struct ll {
	int v;
	struct ll * next;
} LL_NODE;

LL_NODE **adj;
int *incoming;
int n;

void insert_into_ll (LL_NODE* head, int v) {
	LL_NODE* nn = (LL_NODE*) malloc(sizeof(LL_NODE));
	nn->v = v;
	nn->next = NULL;
	LL_NODE* temp = head;
	while(temp->next != NULL)
		temp = temp->next;
	temp->next = nn;
}

void insert_into_ll_beg (LL_NODE* head, int v) {
	LL_NODE* nn = (LL_NODE*) malloc(sizeof(LL_NODE));
	nn->v = v;
	nn->next = head->next;
	head->next = nn;
}

void add_edge(int u, int v) {
	insert_into_ll(adj[u],v);
	incoming[v]++;
}

void dfs(int i,LL_NODE* stack, int* visited) {
	visited[i] = 1;
	LL_NODE* temp = adj[i]->next;
	while(temp) {
		if(!visited[temp->v])
			dfs(temp->v,stack,visited);
		temp = temp->next;
	}
	insert_into_ll_beg(stack,i);
}

void dfs_topo() {
	LL_NODE* stack = (LL_NODE*) calloc(1,sizeof(LL_NODE));
	int* visited = (int*) malloc(n*sizeof(int));
	for(int i=0;i<n;i++) {
		if(!visited[i]) {
			dfs(i,stack,visited);
		}
	}
	LL_NODE *temp = stack->next;
	printf("Using dfs\n");
	while(temp) {
		printf("%d ", temp->v);
		temp = temp->next;
	}
	printf("\n");
	free(stack);
	free(visited);
}

void src_rem_topo() {
	int* visited = (int*) calloc(n,sizeof(int));
	printf("Using src removal\n");
	for(int i=0;i<n;i++) {
		if(incoming[i] == 0 && visited[i]==0) {
			printf("%d ", i);
			visited[i] = 1;
			LL_NODE* temp = adj[i]->next;
			while(temp) {
				incoming[temp->v]--;
				temp = temp->next;
			}
			i=-1;
		}
	}
	printf("\n");
	free(visited);
}

int main() {
	int e;
	printf("Enter total number of vertices : ");
	scanf("%d",&n);
	incoming = (int*) calloc(n,sizeof(int));
	adj = (LL_NODE**) calloc(n,sizeof(LL_NODE*));
	for(int i=0;i<n;i++)
		adj[i] = (LL_NODE*) calloc(n,sizeof(LL_NODE));
	printf("Enter number of edges : ");
	scanf("%d",&e);
	printf("Enter edges\n");
	while(e--) {
		int u,v;
		scanf("%d %d",&u,&v);
		if(u>=n || v>=n || u<0 || v<0) {
			printf("Vertices must lie between 0 and %d\n",n-1);
			return -1;
		}
		add_edge(u,v);
	}
	dfs_topo();
	src_rem_topo();
}