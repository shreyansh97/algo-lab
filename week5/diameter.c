#include <stdio.h>
#include <stdlib.h>

struct tnode {
	int data;
	struct tnode *left;
	struct tnode *right;
	struct tnode *parent;
};

struct ret {
	int distance;
	struct tnode *node;
};

struct tnode* accept(struct tnode *root, struct tnode *parent) {
	if(!root) {
		printf("Enter value of node : ");
		int n;
		scanf("%d",&n);
		root = (struct tnode*) malloc(sizeof(struct tnode));
		root->data = n;
		root->left = NULL;
		root->parent = parent;
		root->right = NULL;
		return root;
	}
	printf("Value at current node is %d, enter l or r :", root->data);
	char ch='1';
	while(ch!='l' && ch!='r')
		scanf("%c",&ch);
	if(ch=='l')
		root->left = accept(root->left, root);
	else
		root->right = accept(root->right, root);
	return root;
}

struct ret max(struct ret a,struct ret b) {
	if(a.distance>b.distance)
		return a;
	return b;
}

struct ret dfs(struct tnode *root, struct tnode *incoming) {
	struct ret ans = {0,root};
	if(root->left && root->left!=incoming)
		ans = max(ans,dfs(root->left, root));
	if(root->right && root->right!=incoming)
		ans = max(ans,dfs(root->right, root));
	if(root->parent && root->parent!=incoming)
		ans = max(ans, dfs(root->parent, root));
	ans.distance = ans.distance+1;
	return ans;
}


int main() {
	int n;
	struct tnode *root = NULL;
	printf("Enter number of nodes : ");
	scanf("%d",&n);
	while(n--){
		printf("\n");
		root = accept(root,NULL);
	}
	struct ret ans1 = dfs(root,NULL);
	struct ret ans2 = dfs(ans1.node,NULL);
	printf("\n\nDiameter is %d\n", ans2.distance);
	return 0;
}