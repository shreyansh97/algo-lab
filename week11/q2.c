#include <stdio.h>
#include <stdlib.h>
#include "heap.h"
#define INF 1e9+7

int main() {
	int e,s,n;
	int** adj;
	printf("Enter number of vertices : ");
	scanf("%d",&n);
	adj = malloc(sizeof(int*)*n);
	for(int i=0;i<n;i++) {
		adj[i] = malloc(sizeof(int)*n);
		for(int j=0;j<n;j++) {
			adj[i][j] = INF;
		}
	}
	printf("Enter number of edges : ");
	scanf("%d",&e);
	printf("Enter edges(u v weight)\n");
	for(int i=0;i<e;i++) {
		int u,v,w;
		scanf("%d",&u);
		scanf("%d",&v);
		scanf("%d",&w);
		adj[u][v] = w;
		adj[v][u] = w;
	}
	printf("Enter source node (0-%d)\n",(n-1));
	scanf("%d",&s);
	assert(s<n && s>=0);
	int *d = malloc(sizeof(int)*n);
	for(int i=0;i<n;i++)
		d[i] = INF;
	d[s] = 0;
	HEAP* heap = get_empty_heap(n);
	insert_element(heap,s);
	while(heap->size > 0) {
		int el = pop(heap);
		for(int i=0;i<n;i++) {
			if(adj[el][i]+d[el] < d[i]) {
				d[i] = adj[el][i]+d[el];
				insert_element(heap,i);
			}
		}
	}
	printf("Distances : \n");
	for(int i=0;i<n;i++)
		printf("%d ", d[i]);
	return 0;
}