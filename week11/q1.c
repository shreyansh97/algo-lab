#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int u,v,w;
} EDGE;

int max(int a,int b) {
	if(a>b)
		return a;
	return b;
}

int comparator(const void* a,const void* b) {
	EDGE n = *(EDGE*)a;
	EDGE m = *(EDGE*)b;
	return n.w - m.w;
}

int find(int *p, int i) {
	if(p[i]!=i)
		p[i] = find(p,p[i]);
	return p[i];
}

int join(int* p,int x,int y) {
	x = find(p,x);
	y = find(p,y);
	if(x==y)
		return 0;
	p[y] = x;
	return 1;
}

int main() {
	int e;
	EDGE* ed;
	printf("Enter number of edges : ");
	scanf("%d",&e);
	ed = malloc(sizeof(EDGE)*e);
	printf("Enter edges(u v weight)\n");
	int n = -1;
	for(int i=0;i<e;i++) {
		scanf("%d",&(ed[i].u));
		scanf("%d",&(ed[i].v));
		scanf("%d",&(ed[i].w));
		n = max(n,max(ed[i].u,ed[i].v));
	}
	n++;
	int *p = malloc(sizeof(int)*n);
	for(int i=0;i<n;i++)
		p[i] = i;
	qsort((void*)ed,e,sizeof(EDGE),comparator);
	int mstw = 0;
	for(int i=0;i<e;i++) {
		if(join(p,ed[i].u,ed[i].v)) {
			printf("%d %d %d\n",ed[i].u,ed[i].v,ed[i].w);
			mstw += ed[i].w;
		}
	}
	printf("Weight of MST is %d\n", mstw);
	return 0;
}