#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nheap.h"

typedef struct _node {
	char val;
	struct _node *left, *right;
	int freq;
}NODE;

int comp(void *a,void *b) {
	NODE n1 = *(NODE*)a;
	NODE n2 = *(NODE*)b;
	return n1.freq - n2.freq;
}

void print(NODE* root,char seq[10]) {
	if(root->val=='\0') {
		int len = strlen(seq);
		seq[len+1] = '\0';
		seq[len] = '0';
		print(root->left,seq);
		seq[len] = '1';
		print(root->right,seq);
		seq[len] = '\0';
	} else {
		printf("%c\t%s\n",root->val,seq);
	}
}

int main() {
	char chars[] = {'a','b','c','d','e','f'};
	int freqs[] = {5,9,12,13,16,45};
	int n = sizeof(chars) / sizeof(char);
	// assert(n>0);
	PQ* pq = initialize(n,sizeof(NODE));

	for(int i=0;i<n;i++) {
		NODE* nn = malloc(sizeof(NODE));
		nn->val = chars[i];
		nn->freq = freqs[i];
		nn->left = NULL;
		nn->right = NULL;
		push(pq,(void*)nn,comp);
	}

	while(pq->head > 1) {
		NODE* n1 = pop(pq,comp);
		NODE* n2 = pop(pq,comp);
		NODE* n3 = malloc(sizeof(NODE));
		n3->val = '\0';
		n3->left = n1;
		n3->right = n2;
		n3->freq = n1->freq + n2->freq;
		push(pq,(void*)n3,comp);
	}

	NODE* root = pop(pq,comp);
	char seq[10] = "";
	print(root,seq);
}