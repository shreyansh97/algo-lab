#include <stdio.h>
#include "open_hashing.h"

int eq_cmp(void* a, void* b) {
	int n = *(int*)a;
	int m = *(int*)b;
	return n==m;
}

int hash(void* a) {
	int n = *(int*)a;
	return n;
}

int main() {
	printf("Menu\n1. Add 2. Find 3. Remove  Any other key to exit\n");
	OPEN_HASHER* oh = initialize_hasher(sizeof(int),10,eq_cmp,hash);
	int ch,n;
	while(1) {
		printf("Choice : ");
		scanf("%d",&ch);
		if(ch==1 || ch==2 || ch==3) {
			printf("Enter number : ");
			scanf("%d",&n);
		}
		switch(ch) {
			case 1: add_hasher(oh,(void*)&n);
			break;
			case 2: n = find_hasher(oh,(void*)&n);
			if(n)
				printf("Element found\n");
			else
				printf("Element not found\n");
			break;
			case 3: remove_hasher(oh,(void*)&n);
			break;
			default : return 0;
		}
	}
}