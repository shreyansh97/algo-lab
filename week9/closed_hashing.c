#include <stdio.h>
#include <stdlib.h>
#define MOD 50

int table[MOD];

int insert(int val) {
	int starting = val%MOD;
	for(int i=0;i<MOD;i++) {
		int p = (i+starting)%MOD;
		if(table[p]==-1) {
			table[p] = val;
			return 1;
		}
	}
	return 0;
}

int find(int val) {
	int starting = val%MOD;
	for(int i=0;i<MOD;i++) {
		int p = (i+starting)%MOD;
		if(table[p]==val)
			return 1;
	}
	return 0;
}

int main(int argc, char const *argv[]) {
	for(int i=0;i<MOD;i++)
		table[i]=-1;
	for(int i=1;i<argc;i++)
		insert(atoi(argv[i]));
	printf("Enter searching value : ");
	int key;
	scanf("%d",&key);
	if(find(key))
		printf("Found\n");
	else
		printf("Not Found\n");
	return 0;
}