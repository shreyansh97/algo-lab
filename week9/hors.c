#include <stdio.h>
#include <string.h>

#define MAX_ASCII 256

void input_string(char str[],int len) {
	fgets(str,len,stdin);
	int l = strlen(str);
	if(str[l-1]=='\n')
		str[l-1]='\0';
}

void get_table(char pat[],int t[MAX_ASCII]) {
	int m = strlen(pat);
	for(int i=0;i<MAX_ASCII;i++)
		t[i] = m;
	for(int i=0;i<m-1;i++)
		t[pat[i]] = m-i-1;
}

int match(char t[],char p[],int *comps) {
	int tbl[MAX_ASCII];
	get_table(p,tbl);
	int n = strlen(t);
	int m = strlen(p);
	int i,j;
	for(i=0;i<=n-m;i++) {
		int f = 1;
		for(j=i+m-1;j>=i;j--) {
			(*comps)++;
			if(p[j-i] != t[j]) {
				i += tbl[t[i+m-1]] - 1;
				f=0;
				break;
			}
		}
		if(f)
			return i;
	}
	return -1;
}

int main() {
	char text[100],pat[100];
	printf("Enter text : \n");
	input_string(text,100);
	printf("Enter matching pattern : \n");
	input_string(pat,100);
	int keys = 0;
	int res = match(text,pat,&keys);
	if(res==-1)
		printf("Unsuccessful\n");
	else
		printf("Successful\n");
	printf("Key searches : %d\n", keys);
}