#ifndef OPEN_HASH
#define OPEN_HASH

#include <stdlib.h>
#include "linked_list.h"

typedef struct {
	LL** hash_table;
	int el_len;
	int numel;
	int (*eq_cmp)(void*,void*);
	int (*hash)(void*);
}OPEN_HASHER;

OPEN_HASHER* initialize_hasher(int el_len, int numel, int (*eq_cmp)(void*, void*), int (*hash)(void*)) {
	OPEN_HASHER* hasher = malloc(sizeof(OPEN_HASHER));
	hasher->el_len = el_len;
	hasher->numel = numel;
	hasher->eq_cmp = eq_cmp;
	hasher->hash = hash;
	hasher->hash_table = malloc(sizeof(LL*) * numel);
	for(int i=0;i<numel;i++)
		hasher->hash_table[i] = initialize_ll(el_len,eq_cmp);
	return hasher;
}

int getidx(OPEN_HASHER* oh, void* data) {
	int pos = oh->hash(data);
	pos %= oh->numel;
	return pos;
}

void add_hasher(OPEN_HASHER* oh, void* data) {
	int i = getidx(oh,data);
	add_to_ll(oh->hash_table[i],data);
}

int find_hasher(OPEN_HASHER* oh, void* data) {
	int i = getidx(oh, data);
	return find_in_ll(oh->hash_table[i],data);
}

void remove_hasher(OPEN_HASHER* oh, void* data) {
	int i = getidx(oh,data);
	remove_from_ll(oh->hash_table[i],data);
}

#endif