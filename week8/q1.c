#include <stdio.h>
#include "heap.h"
#include <stdlib.h>

int main(int argc, char const *argv[]) {
	inscount = 0;
	int* a;
	int n = argc-1;
	a = (int*) malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
		a[i] = atoi(argv[i+1]);
	HEAP* heap = make_top_down(a,n);
	display_heap_arr(*heap);
	printf("Instructions : %d\n", inscount);
	return 0;
}
