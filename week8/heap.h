/**
Describes the functions for a max heap of integers
*/

#ifndef HEAP_LIB
#define HEAP_LIB

#include <stdlib.h>
#include <math.h>
#include <string.h>

#define PT(n) (n>>1)
#define LC(n) (n<<1)
#define RC(n) ((n<<1) + 1)
#define SPC(n) (2*n-1)

typedef struct {
	int* arr;
	int capacity;
	int size;
} HEAP;

int inscount;

HEAP* get_empty_heap(int capacity) {
	HEAP* heap = (HEAP*) malloc(sizeof(HEAP));
	heap->capacity = capacity;
	heap->size = 0;
	heap->arr = (int*) malloc(sizeof(int)*(capacity+1));
	return heap;
}

void swap(HEAP* heap,int i,int j) {
	inscount++;
	int temp = heap->arr[i];
	heap->arr[i] = heap->arr[j];
	heap->arr[j] = temp;
}

void insert_element(HEAP* heap,int elem) {
	// printf("Inserting %d\n", elem);
	if(heap->size == heap->capacity) {
		// printf("triggered\n");
		heap->capacity *= 2;
		heap->arr = realloc(heap->arr, heap->capacity);
	}
	heap->arr[++(heap->size)] = elem;
	int temp = heap->size;
	while(PT(temp) > 0 && heap->arr[PT(temp)] < heap->arr[temp]) {
		swap(heap,temp,PT(temp));
		temp = PT(temp);
		inscount++;
	}
}

void heapify(HEAP* heap, int index) {
	inscount++;
	int max = index;
	int l = LC(index);
	int r = RC(index);
	int sz = heap->size;
	if(l<=sz && heap->arr[l] > heap->arr[max])
		max = l;
	if(r<=sz && heap->arr[r] > heap->arr[max])
		max = r;
	if(max!=index) {
		swap(heap,index,max);
		heapify(heap,max);
	}
}

HEAP* make_bottom_up(int* a,int n) {
	HEAP* heap = get_empty_heap(n);
	heap->size = n;
	for(int i=0;i<n;i++) {
		inscount++;
		heap->arr[i+1] = a[i];
	}
	int i = PT(n);
	while(i>0){
		heapify(heap,i);
		i--;
	}
	return heap;
}

HEAP* make_top_down(int *a, int n) {
	HEAP* heap = get_empty_heap(n);
	for(int i=0;i<n;i++) {
		inscount++;
		insert_element(heap,a[i]);
	}
	return heap;
}

int get_depth(HEAP heap) {
	int num = heap.size;
	int l = (int) floor(log2(num));
	return l;
}

void display_heap(HEAP heap) {
	int last_level = 1<<get_depth(heap);
	int curr_level = 1;
	int i=1;
	while(i<=heap.size) {
		int spaces = (SPC(last_level) - SPC(curr_level)) / 4;
		while(spaces--)
			printf("\t");
		for(int j=0;j<curr_level && i<= heap.size;j++, i++)
			printf("%d\t", heap.arr[i]);
		printf("\n");
		curr_level <<=1;
	}
}

void display_heap_arr(HEAP heap) {
	for(int i=1;i<=heap.size;i++)
		printf("Index %d\tValue %d\n", i,heap.arr[i]);
}

int pop(HEAP* heap) {
	inscount++;
	if(heap->size < 1)
		return -1;
	int toret = heap->arr[1];
	swap(heap,1,heap->size--);
	heapify(heap,1);
	return toret;
}

#endif
