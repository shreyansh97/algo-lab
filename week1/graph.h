/*implement graph representations and display adj list and matrix*/
#ifndef GRAPH
#define GRAPH

#include "double-linked-list.h"

typedef DLLHEAD** ADJLIST;
typedef int** ADJMAT;

ADJLIST create_list(int vertices) {
	ADJLIST list = (ADJLIST) calloc(vertices,sizeof(DLLHEAD*));
	int i;
	for(i=0;i<vertices;i++)
		list[i] = init_dll();
	return list;
}

ADJMAT create_mat(int vertices) {
	ADJMAT mat = (ADJMAT) calloc(vertices,sizeof(int*));
	int i;
	for(i=0;i<vertices;i++)
		mat[i] = (int*) calloc(vertices,sizeof(int));
	return mat;
}


//adds an edge between i and j
void add_edge(ADJLIST list, ADJMAT mat, int i,int j) {
	if(mat[i][j])
		return;
	mat[i][j] = 1;
	mat[j][i] = 1;
	add_at_last(list[i],j);
	add_at_last(list[j],i);
}

void display_list(ADJLIST list,int vertices) {
	int i;
	for(i=0;i<vertices;i++) {
		printf("%d - ", i);
		display_dll(list[i]);
	}
}

void display_mat(ADJMAT mat,int vertices) {
	int i,j;
	for(i=0;i<vertices;i++)
		printf("\t%d",i);
	printf("\n");
	for(i=0;i<vertices;i++) {
		printf("%d\t",i);
		for(j=0;j<vertices;j++) {
			printf("%d\t", mat[i][j]);
		}
		printf("\n");
	}
}

#endif