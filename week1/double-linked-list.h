#ifndef DOUBLE_LINKED_LIST
#define DOUBLE_LINKED_LIST

#include <stdio.h>
#include <stdlib.h>

typedef struct dllnode {
	int data;
	struct dllnode *next;
	struct dllnode *prev;
} DLLNODE;

typedef struct {
	DLLNODE* first;
	DLLNODE* last;
} DLLHEAD;

DLLHEAD* init_dll() {
	DLLHEAD* head = (DLLHEAD*) malloc(sizeof(DLLHEAD));
	head->first = NULL;
	head->last = NULL;
	return head;
}

DLLNODE* create_dll_node(int data, DLLNODE* next, DLLNODE* prev) {
	DLLNODE* node = (DLLNODE*) malloc(sizeof(DLLNODE));
	node->next = next;
	node->prev = prev;
	node->data = data;
	return node;
}

void add_at_last(DLLHEAD* head,int data) {
	DLLNODE* nn = create_dll_node(data,NULL,head->last);
	if(head->last) {
		head->last->next = nn;
	} else {
		head->first = nn;
	}
	head->last = nn;
}

void add_at_first(DLLHEAD* head, int data) {
	DLLNODE* nn = create_dll_node(data,head->first,NULL);
	if(head->first)
		head->first->prev = nn;
	head->first = nn;
	if(!head->last)
		head->last = nn;
}

void insert_to_left(DLLHEAD* head,int insert,int searchkey) {
	if(!head || !head->first)
		return;
	if(head->first->data == searchkey) {
		add_at_first(head,insert);
		return;
	}
	DLLNODE* temp = head->first->next;
	while(temp) {
		if(temp->data == searchkey) {
			DLLNODE* nn = create_dll_node(insert,temp,temp->prev);
			temp->prev->next = nn;
			temp->prev = nn;
			return;
		}
		temp = temp->next;
	}
}

void delete_all(DLLHEAD* head, int key) {
	while(head && head->first && head->first->data == key) {
		DLLNODE* n = head->first;
		if(n->next)
			n->next->prev = NULL;
		head->first = n->next;
		free(n);
	}
	DLLNODE* temp = head->first;
	while(temp) {
		DLLNODE* next = temp->next;
		if(temp->data == key) {
			temp->prev->next = temp->next;
			if(temp->next)
				temp->next->prev = temp->prev;
			free(temp);
		}
		temp = next;
	}
}

void search_key(DLLHEAD* head, int key) {
	DLLNODE* temp = head->first;
	while(temp) {
		if(temp->data == key) {
			printf("Key found\n");
			return;
		}
		temp=temp->next;
	}
	printf("Key not found\n");
}

void display_dll(DLLHEAD* head) {
	DLLNODE* temp = head->first;
	while(temp) {
		printf("%d ",temp->data);
		temp=temp->next;
	}
	printf("\n");
}

#endif