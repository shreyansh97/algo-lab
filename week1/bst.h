/*construct bst to support following operations. No duplicate elements
search key, if found print "key found" else insert
display inorder,preorder,postorder*/
#ifndef BST
#define BST

#include <stdio.h>
#include <stdlib.h>

typedef struct btn {
	int key;
	struct btn* left;
	struct btn* right;
} TNODE;

TNODE* create_node(int key) {
	TNODE *node;
	node = (TNODE*) malloc(sizeof(TNODE));
	node->key = key;
	node->left = NULL;
	node->right = NULL;
	return node;
}

TNODE* find_or_insert(TNODE *root, int key) {
	if(!root)
		return create_node(key);
	if(root->key == key) {
		printf("key found\n");
	} else if(root->key < key) {
		root->right = find_or_insert(root->right,key);
	} else {
		root->left = find_or_insert(root->left,key);
	}
	return root;
}

void inorder(TNODE* root) {
	if(root) {
		inorder(root->left);
		printf("%d ", root->key);
		inorder(root->right);
	}
}

void preorder(TNODE* root) {
	if(root) {
		printf("%d ", root->key);
		preorder(root->left);
		preorder(root->right);
	}
}

void postorder(TNODE* root) {
	if(root) {
		postorder(root->left);
		postorder(root->right);
		printf("%d ", root->key);
	}
}

#endif
