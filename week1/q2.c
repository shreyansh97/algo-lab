#include "graph.h"

int main() {
	int vertices = 10;
	ADJLIST list = create_list(vertices);
	ADJMAT mat = create_mat(vertices);
	add_edge(list,mat,1,5);
	add_edge(list,mat,3,5);
	add_edge(list,mat,0,7);
	add_edge(list,mat,3,9);
	printf("Adj List\n");
	display_list(list,vertices);
	printf("Adj Matrix\n");
	display_mat(mat,vertices);
	return 0;
}