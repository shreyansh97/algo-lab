#include "double-linked-list.h"

int main() {
	DLLHEAD* dll = init_dll();
	add_at_first(dll,5);
	add_at_first(dll,7);
	add_at_first(dll,6);
	add_at_first(dll,7);
	add_at_first(dll,8);
	display(dll);
	insert_to_left(dll,7,5);
	display(dll);
	search_key(dll,8);
	search_key(dll,10);
	delete_all(dll,7);
	display(dll);
	delete_all(dll,8);
	display(dll);
	return 0;
}