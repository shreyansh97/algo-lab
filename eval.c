#include <stdio.h>
#include <stdlib.h>

int *set;
int num;

void print_set() {
	printf("[ ");
	for(int i=0;i<num;i++)
		printf("%d ", set[i]);
	printf("]\n");
}

int check_sum(int *a,int n,int sum) {
	if(sum==0){
		print_set();
		return 1;
	}
	if(sum<0 || n<=0)
		return 0;
	int ret = check_sum(a+1,n-1,sum);
	set[num++] = a[0];
	ret |= check_sum(a+1,n-1,sum-a[0]);
	num--;
	return ret;
}

int main(int argc, char const *argv[])
{
	int n,sum,*a;
	printf("Enter number of elements : ");
	scanf("%d",&n);
	printf("Enter %d elements\n", n);
	a = (int*) malloc(n*sizeof(int));
	set = (int*) malloc(n*sizeof(int));
	num=0;
	for(int i=0;i<n;i++)
		scanf("%d",&a[i]);
	printf("Enter required sum : ");
	scanf("%d",&sum);
	if(check_sum(a,n,sum)) {
		printf("The subsets with the given sum is printed above\n");
	} else {
		printf("There are no subsets with the given sum\n");
	}
	return 0;
}