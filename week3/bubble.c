#include <stdio.h>
#include <stdlib.h>

int bubble_sort(int *a, int n) {
	int opcount = 0,swaps=0;
	for(int i=0;i<n-1;i++) {
		swaps = 0;
		for(int j=0;j<n-i-1;j++) {
			++opcount;
			if(a[j] > a[j+1]) {
				swaps++;
				int t = a[j];
				a[j] = a[j+1];
				a[j+1] = t;
			}
		}
		if(swaps==0)
			break;
	}
	return opcount;
}

int main() {
	int n, *a, i;
	printf("Enter number of elements : ");
	scanf("%d",&n);
	a = (int*) malloc(n * sizeof(int));
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	int opcount = bubble_sort(a,n);
	printf("Operations - %d\n",opcount);
	return 0;
}