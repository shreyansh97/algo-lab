#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int matching(char* t,char* p) {
	int n = strlen(t);
	int m = strlen(p);
	int opcount = 0,i,j;
	for(i=0;i<=n-m;i++) {
		for(j=0;j<m;j++) {
			++opcount;
			if(t[i+j] != p[j])
				break;
		}
		if(j==m) {
			printf("Match found at %d\n",i+1);
			return opcount;
		}
	}
	printf("Pattern not found\n");
	return opcount;
}

int main() {
	char t[256],p[256];
	printf("Enter text : \n");
	// scanf("%[^\n]\ns",t);
	fgets(t,256,stdin);
	printf("Enter pattern : \n");
	// scanf("%[^\n]\ns",p);
	fgets(p,256,stdin);
	int ops = matching(t,p);
	printf("Operations - %d\n",ops);
}

