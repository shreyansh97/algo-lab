#include <stdio.h>
#include <stdlib.h>

int find_partition(int *a,int n,int sum, int i) {
	if(i>=n || sum < 0)
		return 0;
	if(sum==0)
		return 1;
	if(find_partition(a,n,sum-a[i],i+1)) {
		printf("%d ", a[i]);
		return 1;
	}
	return find_partition(a,n,sum,i+1);
}

int main() {
	int n, *a, i;
	printf("Enter number of elements : ");
	scanf("%d",&n);
	a = (int*) malloc(n * sizeof(int));
	int sum = 0;
	for(int i=0;i<n;i++) {
		scanf("%d",a+i);
		sum += a[i];
	}
	if(sum%2==1 || find_partition(a,n,sum/2,0)==0) {
		printf("No such partition exists\n");
	}
	else {
		printf("\n");
	}
	return 0;
}