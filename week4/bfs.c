#include <stdio.h>
#include <stdlib.h>

typedef struct ll {
	int v;
	struct ll * next;
} LL_NODE;

LL_NODE **adj;

void insert_into_ll (LL_NODE* head, int v) {
	LL_NODE* nn = (LL_NODE*) malloc(sizeof(LL_NODE));
	nn->v = v;
	nn->next = NULL;
	LL_NODE* temp = head;
	while(temp->next != NULL)
		temp = temp->next;
	temp->next = nn;
}

void add_edge(int u,int v) {
	insert_into_ll (adj[u],v);
	insert_into_ll (adj[v],u);
}

void bfs(int i,int* visited,int n) {
	int* queue = (int*) malloc(n * sizeof(int));
	int top = 0;
	int front = 0;
	queue[top++] = i;
	visited[i] = 1;
	while(front < top) {
		int curr = queue[front++];
		printf("%d ", curr);
		LL_NODE* temp = adj[curr]->next;
		while(temp) {
			if(!visited[temp->v]) {
				visited[temp->v] = 1;
				queue[top++] = temp->v;
			}
			temp = temp->next;
		}
	}
	free(queue);
}

int main() {
	int n,e;
	printf("Enter total number of vertices : ");
	scanf("%d",&n);
	adj = (LL_NODE**) calloc(n,sizeof(LL_NODE*));
	for(int i=0;i<n;i++)
		adj[i] = (LL_NODE*) calloc(n,sizeof(LL_NODE));
	printf("Enter number of edges : ");
	scanf("%d",&e);
	printf("Enter edges\n");
	while(e--) {
		int u,v;
		scanf("%d %d",&u,&v);
		add_edge(u,v);
	}
	int* visited = (int*) calloc(n,sizeof(int));
	printf("BFS order : \n");
	for(int i=0;i<n;i++) {
		if(!visited[i]) {
			bfs(i,visited,n);
		}
	}
	printf("\n");
}