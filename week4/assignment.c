#include <stdio.h>
#include <stdlib.h>

#define INF 1000000007

int taken[1000];
int cost[1000][1000];
int assignment[1000];
int minassn[1000];
int n;
int min;

int calculate(int p,int cos) {
	int steps = 1;
	if(cos>min)
		return 1;
	if (p>=n) {
		if(cos < min) {
			min = cos;
			for(int i=0;i<n;i++) {
				steps++;
				minassn[i] = assignment[i];
			}
		}
		return steps;
	}
	for(int i=0;i<n;i++) {
		steps++;
		if(!taken[i]) {
			taken[i] = 1;
			assignment[p] = i;
			steps+=calculate(p+1,cos+cost[p][i]);
			taken[i] = 0;
		}
	}
	return steps;
}

int main() {
	printf("Enter number of jobs :\n");
	scanf("%d",&n);
	min = INF;
	for(int i=0;i<n;i++) {
		taken[i] = 0;
		assignment[i] = -1;
		printf("Enter costs for person %d \n", i);
		for(int j=0;j<n;j++){
			scanf("%d",&cost[i][j]);
		}
	}
	int steps = calculate(0,0);
	printf("Minimum cost is %d \n",min);
	for (int i = 0; i < n; ++i) {
		printf("%d ", minassn[i]);
	}
	printf("\n");
	printf("Steps taken %d \n", steps);
	return 0;
}