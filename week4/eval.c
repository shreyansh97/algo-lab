#include <stdio.h>
#include <stdlib.h>

struct tnode {
	int data;
	struct tnode *left;
	struct tnode *right;
};

struct tnode* accept(struct tnode *root) {
	if(!root) {
		printf("Enter value of node : ");
		int n;
		scanf("%d",&n);
		root = (struct tnode*) malloc(sizeof(struct tnode));
		root->data = n;
		root->left = NULL;
		root->right = NULL;
		return root;
	}
	printf("Value at current node is %d, enter l or r :", root->data);
	char ch='1';
	while(ch!='l' && ch!='r')
		scanf("%c",&ch);
	if(ch=='l')
		root->left = accept(root->left);
	else
		root->right = accept(root->right);
	return root;
}

int find(struct tnode *root) {
	if(!root)
		return 1;
	if(root->left && root->data < root->left->data)
		return 0;
	if(root->right && root->data > root->right->data)
		return 0;
	return find(root->left) & find(root->right);
}

int main() {
	int n;
	struct tnode *root = NULL;
	printf("Enter number of nodes : ");
	scanf("%d",&n);
	while(n--){
		printf("\n");
		root = accept(root);
	}
	if(find(root))
		printf("It is BST\n");
	else
		printf("Not a BST\n");
	return 0;
}