#include <stdio.h>
#include <stdlib.h>

typedef struct ll {
	int v;
	struct ll * next;
} LL_NODE;

LL_NODE **adj;

void insert_into_ll (LL_NODE* head, int v) {
	LL_NODE* nn = (LL_NODE*) malloc(sizeof(LL_NODE));
	nn->v = v;
	nn->next = NULL;
	LL_NODE* temp = head;
	while(temp->next != NULL)
		temp = temp->next;
	temp->next = nn;
}

void add_edge(int u,int v) {
	insert_into_ll (adj[u],v);
	insert_into_ll (adj[v],u);
}

void dfs(int i,int* visited,LL_NODE* push, LL_NODE* pop) {
	visited[i] = 1;
	insert_into_ll(push,i);
	LL_NODE *temp = adj[i]->next;
	while(temp) {
		if(!visited[temp->v])
			dfs(temp->v,visited,push,pop);
		temp = temp->next;
	}
	insert_into_ll(pop,i);
}

int main() {
	int n,e;
	printf("Enter total number of vertices : ");
	scanf("%d",&n);
	adj = (LL_NODE**) calloc(n,sizeof(LL_NODE*));
	for(int i=0;i<n;i++)
		adj[i] = (LL_NODE*) calloc(1,sizeof(LL_NODE));
	printf("Enter number of edges : ");
	scanf("%d",&e);
	printf("Enter edges\n");
	while(e--) {
		int u,v;
		scanf("%d %d",&u,&v);
		add_edge(u,v);
	}
	int* visited = (int*) calloc(n,sizeof(int));
	LL_NODE* push =(LL_NODE*) calloc(1,sizeof(LL_NODE));
	LL_NODE* pop = (LL_NODE*) calloc(1,sizeof(LL_NODE));
	for(int i=0;i<n;i++) {
		if(!visited[i]) {
			dfs(i,visited,push,pop);
		}
	}
	printf("Push order\n");
	LL_NODE *temp = push->next;
	while(temp!=NULL) {
		printf("%d ", temp->v);
		temp=temp->next;
	}
	printf("\nPop order\n");
	temp = pop->next;
	while(temp!=NULL) {
		printf("%d ", temp->v);
		temp=temp->next;
	}
	printf("\n");
}