#include <stdio.h>
#include <stdlib.h>
#define INF 100000007

int min(int a,int b) {
	if(a<b)
		return a;
	return b;
}

int main() {
	int n,**adj;
	printf("Enter number of vertices : ");
	scanf("%d",&n);
	printf("Enter distance matrix(-1 for infinity) :\n");
	adj = malloc(sizeof(int*)*n);
	for(int i=0;i<n;i++) {
		adj[i] = malloc(sizeof(int)*n);
		for(int j=0;j<n;j++){
			scanf("%d",&adj[i][j]);
			if(adj[i][j]==-1)
				adj[i][j] = INF;
		}
	}
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<n;k++)
				adj[j][k] = min(adj[j][k], adj[j][i] + adj[i][k]);

	printf("\nAll pairs shortest path : \n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%d ", adj[i][j]==INF?-1:adj[i][j]);
		printf("\n");
	}
	return 0;
}