#include <stdio.h>
#include <stdlib.h>

int main() {
	int n,**adj,opcount=0;
	printf("Enter number of vertices : ");
	scanf("%d",&n);
	printf("Enter adjascency matrix :\n");
	adj = malloc(sizeof(int*)*n);
	for(int i=0;i<n;i++) {
		adj[i] = malloc(sizeof(int)*n);
		for(int j=0;j<n;j++)
			scanf("%d",&adj[i][j]);
	}
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<n;k++){
				adj[j][k] = adj[j][k] | (adj[j][i] & adj[i][k]);
				opcount++;
			}

	printf("\nTransitive closure : \n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%d ", adj[i][j]);
		printf("\n");
	}
	printf("opcount : %d\n", opcount);
	return 0;
}