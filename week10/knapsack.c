#include <stdio.h>
#include <stdlib.h>

int max(int a,int b) {
	if(a>b)
		return a;
	return b;
}

int main() {
	int n, *val, *weight, W;
	printf("Enter number of items : ");
	scanf("%d",&n);
	printf("Enter total weight : ");
	scanf("%d",&W);
	val = malloc(sizeof(int)*n);
	weight = malloc(sizeof(int)*n);
	printf("Enter values :\n");
	for (int i = 0; i < n; ++i)
		scanf("%d",val+i);
	printf("Enter weights :\n");
	for (int i = 0; i < n; ++i)
		scanf("%d",weight+i);
	int **dp = malloc(sizeof(int*)*(n+1));
	for(int i=0;i<=n;i++)
		dp[i] = malloc(sizeof(int)*(W+1));
	for(int i=0;i<=n;i++) {
		dp[i][0] = 0;
		dp[0][i] = 0;
	}
	for(int i=1;i<=n;i++) {
		for(int j=1;j<=W;j++) {
			dp[i][j] = dp[i-1][j];
			if(weight[i]<=j)
				dp[i][j] = max(dp[i][j],val[i]+dp[i-1][j-weight[i]]);
		}
	}
	printf("Maximum value is %d \n", dp[n][W]);
}