#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int val;
	float weight;
}NODE;

float max_val;

int comparator(const void* a, const void* b) {
	NODE n2 = *(NODE*)a;
	NODE n1 = *(NODE*)b;
	return (n1.val/n1.weight) - (n2.val/n2.weight);
}

float upper_bound(NODE* items, int n, float W) {
	float val = 0;
	int i;
	for(i=0;i<n && W>=items[i].weight;i++) {
		val += items[i].val;
		W -= items[i].weight;
	}
	if(W>0 && i<n)
		val += W/(items[i].weight)*(items[i].val);
	return val;
}

void find(NODE* items, int n, float till_now, float W) {
	if(n<=0)
		return;
	if(W<0)
		return;
	float bound = upper_bound(items,n,W);
	if(till_now > max_val)
		max_val = till_now;
	if(bound + till_now < max_val)
		return;
	find(items+1,n-1,till_now+items[0].val,W-items[0].weight);
	find(items+1,n-1,till_now,W);
}

int main() {
	int vals[] = {5,3,7,8,9};
	float wgts[] = {3.0,4.1,8.0,1.7,3.8};
	float W = 20.0;
	int n = sizeof(vals)/sizeof(int);
	NODE* items = malloc(sizeof(NODE)*n);
	for(int i=0;i<n;i++) {
		items[i].val = vals[i];
		items[i].weight = wgts[i];
	}
	qsort((void*)items,n,sizeof(NODE),comparator);
	max_val = 0;
	find(items,n,0.0,W);
	printf("Maximum value : %f\n", max_val);
}