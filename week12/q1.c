#include <stdio.h>
#include <stdlib.h>

int found(int *a, int n, int sum) {
	if(sum == 0)
		return 1;
	if(n <= 0 || sum<0)
		return 0;
	return (found(a+1,n-1,sum) || found(a+1,n-1,sum-a[0]));
}

int main() {
	int n,*a,d;
	printf("Enter size : ");
	scanf("%d",&n);
	a = malloc(sizeof(int)*n);
	printf("Enter elements : \n");
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	printf("Enter required sum : ");
	scanf("%d",&d);
	if(found(a,n,d))
		printf("Subset found\n");
	else
		printf("Subset not found\n");
	return 0;
}