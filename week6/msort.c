#include <stdio.h>
#include <stdlib.h>

int opcount;

void parr(int *a,int n) {
	for(int i=0;i<n;i++)
		printf("%d ", a[i]);
	printf("\n");
}

void sort(int *a, int n) {
	if(n>1) {
		int mid = n/2;
		int ll = mid;
		int lr = n-mid;
		int *l = (int*) malloc(sizeof(int)*ll);
		int *r = (int*) malloc(sizeof(int)*lr);
		for(int i=0;i<ll;i++){
			++opcount;
			l[i] = a[i];
		}
		for(int i=ll;i<n;i++){
			++opcount;
			r[i-ll] = a[i];
		}
		sort(l,ll);
		sort(r,lr);
		int i=0,j=0,c=0;
		while(i<ll && j<lr) {
			++opcount;
			if(l[i] < r[j])
				a[c++] = l[i++];
			else
				a[c++] = r[j++];
		}
		while(i<ll){
			++opcount;
			a[c++] = l[i++];
		}
		while(j<lr){
			++opcount;
			a[c++] = r[j++];
		}
		free(l);
		free(r);
	}
}

int main() {
	int n, *a;
	printf("Enter number of elements : ");
	scanf("%d",&n);
	a = (int*) malloc(n*sizeof(int));
	printf("Enter elements\n");
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	opcount = 0;
	sort(a,n);
	printf("After sorting : \n");
	for (int i = 0; i < n; ++i)
		printf("%d ", a[i]);
	printf("\nOpcount = %d\n",opcount);
	free(a);
}