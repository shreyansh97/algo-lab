#include <stdio.h>
#include <stdlib.h>

int opcount;

void swap(int *a,int *b) {
	int t = *a;
	*a = *b;
	*b = t;
}

int partition(int *a,int n) {
	int p = a[n-1];
	int i = -1;
	for(int j=0;j<n;j++) {
		++opcount;
		if(a[j] <= p) {
			i++;
			swap(a+i,a+j);
		}
	}
	return i;
}

//sorts the first n elements of array a using quick sort
void sort(int* a,int n) {
	if(n>1) {
		++opcount;
		int p = partition(a,n);
		sort(a,p);
		sort(a+p+1,n-p-1);
	}
}

int main() {
	int n, *a;
	printf("Enter number of elements : ");
	scanf("%d",&n);
	a = (int*) malloc(n*sizeof(int));
	printf("Enter elements\n");
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	opcount = 0;
	sort(a,n);
	printf("After sorting : \n");
	for (int i = 0; i < n; ++i)
		printf("%d ", a[i]);
	printf("\nOpcount = %d\n",opcount);
	free(a);
}