#include <stdio.h>
#include <stdlib.h>

int power(int n,int r) {
	if(r<1)
		return 1;
	if(r==1)
		return n;
	int ans = power(n,r>>1);
	ans *= ans;
	if(r%2==1)
		ans*=n;
	return ans;
}

int main(int argc, char const *argv[])
{
	if(argc!=3) {
		printf("Format ./<progname> n a\n");
		return 0;
	}
	int n = atoi(argv[1]);
	int r = atoi(argv[2]);
	printf("%d\n", power(n,r));
	return 0;
}