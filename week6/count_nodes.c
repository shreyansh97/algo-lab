#include <stdio.h>
#include <stdlib.h>

int opcount;

struct tnode {
	int data;
	struct tnode *left;
	struct tnode *right;
};

struct tnode* accept(struct tnode *root) {
	if(!root) {
		printf("Enter value of node : ");
		int n;
		scanf("%d",&n);
		root = (struct tnode*) malloc(sizeof(struct tnode));
		root->data = n;
		root->left = NULL;
		root->right = NULL;
		return root;
	}
	printf("Value at current node is %d, enter l or r :", root->data);
	char ch='1';
	while(ch!='l' && ch!='r')
		scanf("%c",&ch);
	if(ch=='l')
		root->left = accept(root->left);
	else
		root->right = accept(root->right);
	return root;
}

int count(struct tnode *root) {
	opcount++;
	if(root == NULL)
		return 0;
	int ans = 1;
	ans += count(root->left);
	ans += count(root->right);
	return ans;
}

int main() {
	int n;
	struct tnode *root = NULL;
	printf("Enter number of nodes : ");
	scanf("%d",&n);
	while(n--){
		printf("\n");
		root = accept(root);
	}
	opcount = 0;
	n = count(root);
	printf("Total number of nodes = %d\n", n);
	printf("Operations needed = %d\n", opcount);
	return 0;
}