#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

typedef struct node {
	int info;
	struct node *left, *right;
}NODE;

NODE* create_node(int x) {
	NODE* node = (NODE*) malloc(sizeof(NODE));
	node->info = x;
	node->left = node->right = NULL;
	return node;
}

NODE* left_rotate(NODE* x) {
	NODE* y = x->right;
	NODE* z = y->left;

	y->left = x;
	x->right = z;

	return y;
}

NODE* right_rotate(NODE* y) {
	NODE* x = y->left;
	NODE* z = x->right;

	x->right = y;
	y->left = z;

	return x;
}

int max(int a,int b) {
	if(a>b)
		return a;
	return b;
}

int height(NODE* root) {
	if(root==NULL)
		return 0;
	return 1+max(height(root->left),height(root->right));
}

int bfactor(NODE* root) {
	if(!root)
		return 0;
	return height(root->left) - height(root->right);
}

NODE* insert(NODE* root, int val) {
	if(!root)
		return create_node(val);
	SNODE* st = st_init();
	NODE* temp = root;
	while(temp!=NULL) {
		st_push(st,(void*)temp);
		if(temp->info < val)
			temp = temp->right;
		else if(temp->info > val)
			temp = temp->left;
		else
			return root;
	}
	NODE* prev = create_node(val);
	while(!st_empty(st)) {
		NODE* node = (NODE*) st_pop(st);
		if(node->info < prev->info)
			node->right = prev;
		else
			node->left = prev;
		prev = node;
		int bf = bfactor(node);
		if(bf<2 && bf>-2)
			continue;
		//LL Case
		if(bf > 1 && val < node->left->info)
			prev = right_rotate(node);

		//RR
		else if(bf < -1 && val > node->right->info)
			prev = left_rotate(node);

		//LR
		else if(bf > 1) {
			node->left = left_rotate(node->left);
			prev = right_rotate(node);
		}

		//RL
		else {
			node->right = right_rotate(node->right);
			prev = left_rotate(node);
		}
	}
	return prev;
}

void dfs(NODE* root) {
	if(!root)
		return;
	dfs(root->left);
	printf("%d\t%d\n", root->info, bfactor(root));
	dfs(root->right);
}

void main() {
	int n,x,i;
	NODE* root;
	root = NULL;
	printf("Enter number of nodes\n");
	scanf("%d",&n);
	printf("Enter value at nodes\n");
	for(i=0;i<n;i++) {
		scanf("%d",&x);
		root = insert(root,x);
	}
	printf("Value\tBalance Factor\n");
	dfs(root);
}