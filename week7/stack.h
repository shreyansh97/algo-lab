#ifndef STACK
#define STACK

typedef struct st {
	void* data;
	struct st* next;
}SNODE;

// typedef SNODE* SNODE*;

SNODE* st_init() {
	SNODE* st = (SNODE*) calloc(1,sizeof(SNODE));
}

void st_push(SNODE* st, void* data) {
	SNODE* node = (SNODE*) malloc(sizeof(SNODE));
	node->data = data;
	node->next = st->next;
	st->next = node;
}

int st_empty(SNODE* st) {
	return st->next == NULL;
}

void* st_top(SNODE* st) {
	return st->next->data;
}

void* st_pop(SNODE* st) {
	if(!st->next)
		return NULL;
	SNODE* node = st->next;
	void* data = node->data;
	st->next = node->next;
	free(node);
	return data;
}

#endif