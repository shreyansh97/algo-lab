#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int info;
	struct node *left, *right;
}NODE;

NODE* create(NODE* bnode, int x) {
	NODE *getnode;
	if(bnode==NULL) {
		bnode=(NODE*) malloc(sizeof(NODE));
		bnode->info = x;
		bnode->left = bnode->right = NULL;
	} else if(x > bnode->info)
		bnode->right = create(bnode->right,x);
	else if(x < bnode->info)
		bnode->left = create(bnode->left,x);
	return bnode;
}

int max(int a,int b) {
	if(a>b)
		return a;
	return b;
}

int height(NODE* root) {
	if(root==NULL)
		return 0;
	return 1+max(height(root->left),height(root->right));
}

int bfactor(NODE* root) {
	if(!root)
		return 0;
	return height(root->left) - height(root->right);
}

void dfs(NODE* root) {
	if(!root)
		return;
	dfs(root->left);
	printf("%d\t%d\n", root->info, bfactor(root));
	dfs(root->right);
}

void main() {
	int n,x,ch,i;
	NODE* root;
	root = NULL;
	printf("Enter number of nodes\n");
	scanf("%d",&n);
	printf("Enter value at nodes\n");
	for(i=0;i<n;i++) {
		scanf("%d",&x);
		root = create(root,x);
	}
	printf("Value\tBalance Factor\n");
	dfs(root);
}