#include <stdio.h>

int main() {
	int a,b;
	printf("Enter 2 numbers\n");
	scanf("%d",&a);
	scanf("%d",&b);
	int gcd = 1;
	int min = a;
	int opcount = 0;
	if(b<a)
		min = b;
	for(int i=2;i<=min;i++){
		++opcount;
		if(a%i==0 && b%i==0)
			gcd = i;
	}
	printf("GCD is %d\n", gcd);
	printf("Operation count is %d\n", opcount);
}