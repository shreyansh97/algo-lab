#include <stdio.h>
#include <stdlib.h>
int opcount = 0;
int find(int* a,int n, int key) {
	int i;
	for(i=0;i<n;i++) {
		opcount++;
		if(a[i] == key) {
			a[i] = 0;
			return 1;
		}
	}
	return 0;
}

int main() {
	int a,b;
	printf("Enter 2 numbers\n");
	scanf("%d",&a);
	scanf("%d",&b);
	int gcd = 1;
	int factors[100];
	int h=0;
	int f = 2;
	while(a>1) {
		opcount++;
		while(a%f == 0) {
			factors[h++] = f;
			opcount++;
			a/=f;
		}
		f++;
	}
	// for(a=0;a<h;a++)
	// 	printf("%d ",factors[a]);
	// printf("\n");
	f = 2;
	while(b>1) {
		opcount++;
		while(b%f == 0) {
			opcount++;
			if(find(factors,h,f))
				gcd*=f;
			b/=f;
		}
		f++;
	}
	printf("GCD is %d\n", gcd);
	printf("Operation count is %d\n", opcount);
}